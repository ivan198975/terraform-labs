resource "google_compute_network" "vpc-network" {
    name = "vpc-teste"
    auto_create_subnetworks = false   
    project = "${var.project_id}"

}

resource "google_compute_subnetwork" "subnetwork" {
    name = "sub-teste"
    region = "${var.region}"
    network = google_compute_network.vpc-network.id
    ip_cidr_range = "10.141.1.0/24"
}