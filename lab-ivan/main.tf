resource "google_compute_instance" "vm_instance" {
project      = "${var.project_id}"
name         = "kube-master"
machine_type = "${var.type_machine}"
zone         = "${var.region-zone}"

    boot_disk{
        initialize_params{
            image = "ubuntu-os-cloud/ubuntu-1604-lts"
        }

    }

    network_interface {
        network = "default"
    }

    service_account {
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
    }  

}