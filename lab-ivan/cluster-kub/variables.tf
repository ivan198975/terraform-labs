variable "project_id" {
    default     = ""
    type        = string
    description = "projeto que sera criado a infra"
}

variable "region-zone" {
    default     = "us-east1-b"
    type        = string
    description = "criara na regiao e zona especifica"
}

variable "region" {
    default     = "us-east1"
    type        = string
    description = "criada para as regioes disponiveis"
}

variable "location" {
    default = "US"
    type = string
    description = "Regiao principal para criacao de buckets"
}

variable "type_machine" {
    default     = "n1-standard-1"
    type        = string
    description = "padrao de maquinas para criar"
}