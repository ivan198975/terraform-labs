resource "google_storage_bucket" "bucket" {
    name = "bucket-state"
    location = "${var.location}"
    force_destroy = true

    bucket_policy_only = true 
}