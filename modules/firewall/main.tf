# modulo de firewall basico

resource "google_compute_firewall" "lab_firewall" {
  name     = "terraform-firewall"
  network  = "module.google_compute_network.vpc-instance.name"
  
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "8080"]
  }

  source_tags = ["web"]
}

output "network" {
    value = "module.vpc-instance"
}

output "terraform" {
    value = "module.instance"
}

