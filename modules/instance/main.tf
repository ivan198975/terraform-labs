resource "google_compute_instance" "terraform" {
  name         = "terraform-lab"
  machine_type = "n1-standard-1"
  zone         = var.region
  tags = [
    "name", "Pantera_Negra"
  ]
    
  disk {
    source_image = "centos-cloud/centos-7"
    auto_delete  = true
    disk_size_gb = 20
    boot         = true
  }
  
  network_interface {
    network    =  var.network
  }
  
  
  metadata = {
    foo = "bar"
  }

  can_ip_forward = true
}

output "vpc-instance" {
  value = "module.vpc-instance"
}