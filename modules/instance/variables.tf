variable "network" {
    type        = string
    default     = "terraform-lab-286203-vpc-instance"
    description = "rede vpc projeto terraform-labs"
}

variable "subnet" {
    type        = string
    default     = "terraform-lab-286203-subnet-instance"
    description = "rede subnet projeto terraform labs" 
}

variable  "region" {
    default = "us-east1-b"
}