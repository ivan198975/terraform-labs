variable "project_id" {
    type        = string
    description = "descrição e ID do projeto"
    default     = ""
}

variable "region" {
    type        = string
    description = "região padrão par criação de recursos"
    default     = "us-east1"
}