resource "google_compute_network" "vpc-instance" {
    name                    = "${var.project_id}-vpc-instance"
    auto_create_subnetworks = "false"

}

resource "google_compute_subnetwork" "subnet-instance" {
    name          = "${var.project_id}-subnet-instance"
    region        = var.region
    network       = google_compute_network.vpc-instance.name
    ip_cidr_range = "10.141.1.0/28"
}

