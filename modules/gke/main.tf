resource "google_container_cluster" "primario" {
    name     = "${var.project_id}-gke"
    location = var.region

    remove_default_node_pool = true
    initial_node_count = 1

<<<<<<< HEAD
    #network     = google_compute_network.vpc.vpc-gke.name
    #subnetwork  = google_compute_subnetwork.vpc.subnet-gke.name    
=======
    
>>>>>>> rcouto-release
        
    master_auth {
        username = var.gke_username
        password = var.gke_password

        client_certificate_config {
            issue_client_certificate = false
        }
    
    }
}


resource "google_container_node_pool" "primario_node" {
    name = "google_container_cluster.primario"
    location = var.region
    cluster = google_container_cluster.primario.name
    node_count = var.gke_num_nodes 


    node_config {
      oauth_scopes = [   
        "https://www.googleapis.com/auth/logging.write",
        "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
        env = var.project_id
    }

    machine_type = "n1-standard-1"
    tags = ["gke-nodes", "${var.project_id}-gke"]
    metadata = {
        disable-legacy-endpoints = "true"
      }   

  }  

}

output "kubernetes_cluster_name" {
    value = "google_container_cluster_primario.name"
    description = "Nome do cluster"
}

output "vpc_gke" {
    value = "module.vpc"
}