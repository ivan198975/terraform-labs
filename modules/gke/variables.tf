variable "gke_username" {
    default     = ""
    description = "gke-username"
}

variable "gke_password" {
    default = ""
    description = "gke_password"
}

variable "gke_num_nodes" {
    default     = 3
    description = "numeros de nodes no cluster"
}

variable "project_id" {
    type        = string
    description = "descricao e ID do projeto"
    default     = ""
}

variable "region" {
    type        = string
    description = "regiao padrao par criacao de recursos"
    default     = "us-east1-b"
}

