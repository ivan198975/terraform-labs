
variable "project_id" {
    type        = string 
    default     = ""
    description = "projeto padrão do terraform lab"
}

variable "region" {
    default     = "us-east1-b"
    description = "regiao padrão do projeto lab terraform"
}