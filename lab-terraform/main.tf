resource "google_compute_instance" "lab" {
  name           = "terraform-lab-0"
  machine_type   = "n1-standard-1"
  zone           = "us-east1-b"
    
  tags = ["ex", "dev"]
    
  
    
  boot_disk {
      initialize_params {
        image = "centos-cloud/centos-7"
        size  = "20"
         
      }
    
  }

  network_interface {
      network    = "default"
      #subnetwork = "test-subnetwork"    
  }
}
  /*resource "google_compute_network" "network_vpc" {
  name                    = "default"
  auto_create_subnetworks = true
}*/

/*resource "google_compute_subnetwork" "network_subnet" {
  name          = "test-subnetwork"
  ip_cidr_range = "10.140.1.0/24"
  region        = "us-east1"
  network       = google_compute_network.network_vpc.id
  
}

# modulo de firewall basico

resource "google_compute_firewall" "lab_firewall" {
  name     = "terraform-firewall"
  network  = google_compute_network.network_vpc.name
  
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "8080"]
  }

  allow {
      protocol = "upd"
      ports    = ["1024-65535"] 
  }

  
}*/